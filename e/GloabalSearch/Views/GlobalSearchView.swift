//
//  GlobalSearchView.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/30/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct GlobalSearchView: View, SearhableViewDelegate {

    @State var searchFilter: EventSearchFilter = EventSearchFilter()
    @State private var showFilterView = false
    
    @ObservedObject var viewModel: GlobalSearchViewModel = GlobalSearchViewModel()
    
    var searchServiceDelegate: GlobalSearchDelegate
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    ForEach(viewModel.events, id: \.id) { item in
                        VStack {
                            EventPreviewCell(eventPreviewViewModel: item)
                                .padding(10.0)
                            Spacer(minLength: 10.0)
                        }
                    }
                    Spacer(minLength: 15.0)
                    Button(action: {
                        self.nextPage()
                    }) {
                        Text("More Events")
                    }
                    Spacer(minLength: 15.0)
                }
            }
            .sheet(isPresented: $showFilterView, onDismiss: {
                print(self.searchFilter)
            }) {
                GlobalSearchFilterView(searchDelegate: self)
            }
            .navigationBarItems(trailing:
                HStack {
                    Button(action: {
                        self.showSearchFilterView()
                    }) {
                         Image(systemName: "slider.horizontal.3").imageScale(.large)
                    }
                }
            )
        }.onAppear {
            self.searchServiceDelegate.search(filter: self.searchFilter, success: self.dataReceived, failed: self.errorHappend)
        }
    }
    
    //***SearhableViewDelegate***
    func updateFilter(filter: EventSearchFilter) {
        searchFilter = filter
        showFilterView = false
        searchServiceDelegate.search(filter: self.searchFilter, success: self.dataReceived, failed: self.errorHappend)
    }
    //***SearhableViewDelegate***
    
    func nextPage() {
        self.searchFilter.page += 1
        self.searchServiceDelegate.search(filter: self.searchFilter, success: self.dataReceived, failed: self.errorHappend)
    }
    
    //Searhing callbacks
    func dataReceived(data: Array<EventPreview>?) {
        DispatchQueue.main.async() {
        if let evnts = data {
                if self.searchFilter.page == 0 {
                    self.viewModel.reInit(data: evnts)
                } else {
                    self.viewModel.merge(data: evnts)
                }
            }
        }
    }
    
    func errorHappend(message: String) {
        print(message)
    }
    //***********
    
    
    
    //View logic
    private func showSearchFilterView() {
        self.showFilterView = true
    }
    //***********
}

struct GlobalSearchView_Previews: PreviewProvider {
    static var previews: some View {
        GlobalSearchView(searchServiceDelegate: GlobalSearchService())
    }
}
