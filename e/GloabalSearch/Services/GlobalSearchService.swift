//
//  GlobalSearchService.swift
//  e
//
//  Created by Oleksandr Lisenko on 25.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class GlobalSearchService: GlobalSearchDelegate {
    
    private let globalSearchRepositry = GlobalSearchRepository()
    
    func search(filter: EventSearchFilter, success: @escaping (Array<EventPreview>?) -> (), failed: @escaping (String) -> ()) {
        self.globalSearchRepositry.search(filter: filter, callBack:  { (data: Array<EventPreview>?, error: Error?) in
            if let er = error {
                failed(er.localizedDescription)
            }
            else {
                if let evnts = data {
                    success(evnts)
                }
            }
        })
    }
}
