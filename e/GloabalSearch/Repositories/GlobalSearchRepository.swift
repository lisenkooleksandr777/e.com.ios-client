//
//  GlobalSearchRepository.swift
//  e
//
//  Created by Oleksandr Lisenko on 27.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

internal class GlobalSearchRepository: BaseRepository {
    
    private let globalSearchPath = "/search/global"
    
    public func search(filter: EventSearchFilter, callBack: @escaping (Array<EventPreview>?, Error?) -> Void) {
        
        let url = URL(string: "\(Configuration.SearchSerivceEndPoint!)\(self.globalSearchPath)")!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        
        super.post(data: filter, url: url, headers: headers, completion: callBack)
    }
}
