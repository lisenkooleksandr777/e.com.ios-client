//
//  GlobalSearchProtocolDelegate.swift
//  e
//
//  Created by Oleksandr Lisenko on 26.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

protocol GlobalSearchDelegate {
    func search(filter: EventSearchFilter, success:  @escaping (_: Array<EventPreview>?)->(), failed: @escaping (_: String)->())
}
