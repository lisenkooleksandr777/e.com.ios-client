//
//  GlobalSearchViewModel.swift
//  e
//
//  Created by Oleksandr Lisenko on 26.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI
import Combine

class GlobalSearchViewModel: ObservableObject {
    
    @Published var events: Array<EventPreviewViewModel> = Array<EventPreviewViewModel>()
    
    func reInit(data: Array<EventPreview>) {
        self.clear()
        
        for event in data {
            self.events.append(EventPreviewViewModel(data: event))
        }
    }
    
    func merge(data: Array<EventPreview>) {
        for event in data {
            self.events.append(EventPreviewViewModel(data: event))
        }
    }
        
    private func clear() {
        self.events = Array<EventPreviewViewModel>();
    }
}
