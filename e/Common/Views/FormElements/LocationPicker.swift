//
//  LocationPicker.swift
//  e
//
//  Created by Oleksandr Lisenko on 19.10.2019.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import CoreLocation
import MapKit
import LocationPicker
import SwiftUI
import Combine

final class LocationPicker : ObservableObject {
    
    static let shared : LocationPicker = LocationPicker()
  
    private init() {}  //force using the singleton: LocationPicker.shared
  
    let view = LocationPicker.View()
    let coordinator = LocationPicker.Coordinator()
    
    // Bindable Object part
    var didChange = PassthroughSubject<Location?, Never>()
    
    @Published var location: Location? = nil {
        didSet {
            if location != nil { didChange.send(location) }
        }
    }
    
}

extension LocationPicker {
  class Coordinator: NSObject, UINavigationControllerDelegate {
    
  }
}

extension LocationPicker {
    
    struct View: UIViewControllerRepresentable {
    
        func makeCoordinator() -> Coordinator {
          LocationPicker.shared.coordinator
        }
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<LocationPicker.View>) -> LocationPickerViewController {
          let locationPicker = LocationPickerViewController()

          // button placed on right bottom corner
          locationPicker.showCurrentLocationButton = true // default: true

          // default: navigation bar's `barTintColor` or `.whiteColor()`
          locationPicker.currentLocationButtonBackground = .blue

          // ignored if initial location is given, shows that location instead
          locationPicker.showCurrentLocationInitially = true // default: true

          locationPicker.mapType = .standard // default: .Hybrid

          // for searching, see `MKLocalSearchRequest`'s `region` property
          locationPicker.useCurrentLocationAsHint = true // default: false

          locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"

          locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"

          locationPicker.searchBarStyle = .prominent
          locationPicker.searchTextFieldColor = .white
            
          locationPicker.completion = { location in
            LocationPicker.shared.location = location
        
          }
            
          return locationPicker
        }
    
        func updateUIViewController(_ uiViewController: LocationPickerViewController, context: UIViewControllerRepresentableContext<LocationPicker.View>) {}
    }
}


