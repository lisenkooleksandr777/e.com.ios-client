//
//  EventPreviewCell.swift
//  e
//
//  Created by Oleksandr Lisenko on 27.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct EventPreviewCell: View {
    
    @Environment(\.imageCache) var cache: ImageCache
    var eventPreviewViewModel: EventPreviewViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            AsyncImage(
                url:  eventPreviewViewModel.logoURL,
                cache: self.cache,
                placeholder: Text("Loading ..."),
                configuration: { $0.resizable() }
             )
            .frame(idealHeight: UIScreen.main.bounds.width)
            
            NavigationLink(destination: EventView(previewModel: eventPreviewViewModel)) {
                Text(self.eventPreviewViewModel.name)
                    .font(.headline)
                    .frame(alignment: .leading)
                    .foregroundColor(Color.red)
            }
            
            Spacer(minLength: 5.0)
            
            Button(action: {
                
            }) {
                Image(systemName: "map").imageScale(.medium)
                Text(eventPreviewViewModel.location.name)
                    .font(.subheadline)
            }
            
            HStack {
                Button(action: {
                }) {
                    Image(systemName: "heart.fill").imageScale(.medium)
                }
                Button(action: {
                    
                }) {
                    Text("0")
                        .font(.subheadline)
                }
                
                
                Button(action: {
                }) {
                    Image(systemName: "bookmark").imageScale(.medium)
                }
                
                Button(action: {
                }) {
                    Text("0")
                        .font(.subheadline)
                }
                
                Spacer()
                
                Image(systemName: "calendar")
                
                Text(eventPreviewViewModel.dateRange)
                    .font(.subheadline)
                    .frame(alignment: .trailing)
                
            }
        }
    }
}

