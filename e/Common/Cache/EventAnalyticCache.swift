//
//  EventAnalyticCache.swift
//  e
//
//  Created by Oleksandr Lisenko on 29.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import UIKit

protocol EventAnalyticCache {
    subscript(_ key: String) -> EventAnalytic? { get set }
}

struct TemporaryEventAnalyticCache: EventAnalyticCache {
    
    private let cache = NSCache<NSString, EventAnalytic>()
    
    subscript(key: String) -> EventAnalytic? {
        get {
            cache.object(forKey: key as NSString)
        }
        set {
            newValue == nil ? cache.removeObject(forKey: key as NSString) : cache.setObject(newValue!, forKey: key as NSString)
        }
    }
}
