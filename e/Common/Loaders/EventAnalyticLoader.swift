//
//  EventAnalyticRepository.swift
//  e
//
//  Created by Oleksandr Lisenko on 29.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

import Combine
import UIKit

class EventAnalyticLoader: ObservableObject {
    @Published var eventAnalytic: EventAnalytic?
    
    private(set) var isLoading = false
    
    private let eventId: String
    private var cache: EventAnalyticCache?
    private var cancellable: AnyCancellable?
    
    private static let imageProcessingQueue = DispatchQueue(label: "event-analytic-processing")
    
    init(eventId: String, cache: EventAnalyticCache? = nil) {
        self.eventId = eventId
        self.cache = cache
    }
    
    deinit {
        cancellable?.cancel()
    }
    
    func load() {
        guard !isLoading else { return }

        if let model = cache?[eventId] {
            self.eventAnalytic = model
            return
        }
        
        /*
        cancellable = URLSession.shared.dataTaskPublisher(for: eventId)
            .map { UIImage(data: $0.data) }
            .replaceError(with: nil)
            .handleEvents(receiveSubscription: { [weak self] _ in self?.onStart() },
                          receiveOutput: { [weak self] in self?.cache($0) },
                          receiveCompletion: { [weak self] _ in self?.onFinish() },
                          receiveCancel: { [weak self] in self?.onFinish() })
            .subscribe(on: Self.imageProcessingQueue)
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
         */
    }
    
    func cancel() {
        cancellable?.cancel()
    }
    
    private func onStart() {
        isLoading = true
    }
    
    private func onFinish() {
        isLoading = false
    }
    
    private func cache(_ model: EventAnalytic?) {
        eventAnalytic.map { cache?[eventId] = $0 }
    }
}
