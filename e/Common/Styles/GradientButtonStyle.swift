//
//  GradientButtonStyle.swift
//  e
//
//  Created by Oleksandr Lisenko on 29.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct GradientButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .foregroundColor(Color.white)
            .padding()
            .background(Color.init(red: 255, green: 74, blue: 82))
            .cornerRadius(15.0)
            .scaleEffect(configuration.isPressed ? 1.3 : 1.0)
    }
}
