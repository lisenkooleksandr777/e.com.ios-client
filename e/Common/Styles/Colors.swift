//
//  Colors.swift
//  e
//
//  Created by Oleksandr Lisenko on 22.08.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import UIKit

public class Colors {
    static let loginButtonsBackground = UIColor(red: 0.02, green: 0.412, blue: 0.957, alpha: 1)
    static let loginButtonsBackgroundHover = UIColor(red: 0.251, green: 0.553, blue: 0.965, alpha: 1)
    
    static let tabsButtonsBackground  = UIColor(red: 0.42, green: 0.647, blue: 0.961, alpha: 0.6)
    static let tabsButtonsBackgroundHover  = UIColor(red: 0.02, green: 0.412, blue: 0.957, alpha: 1)
}
