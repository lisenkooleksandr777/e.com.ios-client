//
//  Configuration.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class Configuration {
    
    static var AuthServiceEndPoint: String!
    static var AccountSerivceEndPoint: String!
    static var FileSerivceEndPoint: String!
    static var SearchSerivceEndPoint: String!
    static var AnalyticSerivceEndPoint: String!
    
    static func Configure() {
        Configuration.ConfigureServices()
    }
    
    private static func ConfigureServices() {
        if let services: [String: String] = getPlist(withName: "Services") {
            Configuration.AuthServiceEndPoint = services["AUTH_SERVICE_END_POINT"]
            Configuration.AccountSerivceEndPoint = services["ACCOUNT_SERVICE_END_POINT"]
            Configuration.FileSerivceEndPoint = services["FILE_SERVICE_END_POINT"]
            Configuration.SearchSerivceEndPoint = services["SEARCH_SERVICE_END_POINT"]
            Configuration.AnalyticSerivceEndPoint = services["ANALYTIC_SERVICE_END_POINT"]
        }
    }
    
    private static func getPlist(withName name: String) -> [String:String]?
    {
        if  let path = Bundle.main.path(forResource: name, ofType: "plist"),
            let xml = FileManager.default.contents(atPath: path),
            let cofig = try? PropertyListDecoder().decode([String:String].self, from: xml)
            {
                return cofig
            }
        return nil
    }
}
