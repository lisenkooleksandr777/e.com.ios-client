//
//  Geolocation.swift
//  e
//
//  Created by Oleksandr Lisenko on 27.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import MapKit

public class CurrentGeolocation {
    static let locManager = CLLocationManager()
    
    static func getLocation() -> EventLocation {
        self.locManager.requestWhenInUseAuthorization()
        
        var location = EventLocation()
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            if let currentLocation = locManager.location {
                location.coordinates = [Double]()
                location.coordinates?.append(currentLocation.coordinate.longitude)
                location.coordinates?.append(currentLocation.coordinate.latitude)
                location.gType = "Point"
            }
        }
        return location;
    }
}
