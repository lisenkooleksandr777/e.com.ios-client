//
//  UserSessionHolder.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class SessionDataHolder {
    static var Claims: JWTUserClaims!
    static var JWTToken: String!
}
