//
//  HttpHeaderBuilder.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

internal class HttpHeaderBuilder {
    
    private var headers = [String: String]()
    
    internal func Result() -> [String: String] {
        return self.headers
    }
    
    internal func addJsonContentType() -> HttpHeaderBuilder {
        self.headers["Content-Type"] = "application/json"
        return self
    }
    
    internal func addAuthenticationToken() -> HttpHeaderBuilder {
        self.headers["Authorization"] = "Bearer \(SessionDataHolder.JWTToken!)"
        return self
    }
    
    internal func addMultipartFormDataHeader() -> HttpHeaderBuilder {
        self.headers["Content-type"] = "multipart/form-data"
        return self
    }
}
