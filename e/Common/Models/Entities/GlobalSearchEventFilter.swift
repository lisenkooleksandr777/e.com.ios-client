//
//  GlobalSearchEventFilter.swift
//  e
//
//  Created by Oleksandr Lisenko on 26.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

struct EventSearchFilter: Codable  {
    var location: EventLocation?
    var dateStartAfter: String?
    var dateStartBefore: String?
    var tags: Array<String>?
    var radius: Int = 100
    var page: Int32 = 0
    
    init () {
        location = CurrentGeolocation.getLocation()
        
        let currentDate = Date()
        dateStartAfter = DateTimeUtil.formatDateISO8601(date: currentDate)
        
        var dateComponent = DateComponents()
        dateComponent.month = 1
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        dateStartBefore = DateTimeUtil.formatDateISO8601(date: futureDate!)
    }
}

