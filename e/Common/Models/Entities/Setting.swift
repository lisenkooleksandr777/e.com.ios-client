//
//  UserSetting.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class Setting: Codable {
    let Range: Int32 = 32
    let DaysForward: Int32 = 7
}
