//
//  GeoLocation.swift
//  e
//
//  Created by Oleksandr Lisenko on 19.10.2019.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation
struct EventLocation: Codable {
    public var name: String!
    public var coordinates: [Double]?
    public var detailedLocation: String?
    public var gType: String?
    public var url: String?
}
