//
//  EventLikesMarkedAnalytic.swift
//  e
//
//  Created by Oleksandr Lisenko on 26.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class EventAnalytic {
    public var isLiked: Bool = false
    public var isMarked: Bool = false
    public var likeCount: Int32 = 0
    public var markCount: Int32 = 0
}
