//
//  EventPreview.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/30/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

struct EventPreview: Codable {
    public var id: String!
    public var name: String!
    public var logoURL: String!
    public var location: EventLocation!
    public var dateStart: String!
    public var dateEnd: String!
}
