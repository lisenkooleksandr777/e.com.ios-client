//
//  EventPreviewViewModel.swift
//  e
//
//  Created by Oleksandr Lisenko on 28.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//


import SwiftUI
import Combine

class EventPreviewViewModel: ObservableObject {
    
    public var id: String!
    public var name: String!
    public var logoURL: URL!
    public var location: EventLocation!
    public var dateRange: String!
    
    init(data: EventPreview) {
        self.id = data.id
        self.name = data.name
        self.location = data.location
        self.logoURL = URL(string: "\(Configuration.FileSerivceEndPoint!)/\(data.logoURL!)")!
        self.dateRange = DateTimeUtil.GetISO8601DateRange(from: data.dateStart, to: data.dateStart)
    }
}
