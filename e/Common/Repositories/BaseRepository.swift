//
//  BaseRepository.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

internal class BaseRepository {
    

    //dataRequest which sends request to given URL and convert to Decodable Object
    internal func get<TResponse: Decodable>(url: URL, headers: [String: String], completion: @escaping (TResponse?, Error?) -> Void) {
        
        let session = URLSession.shared

        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 12)
        request.allHTTPHeaderFields = headers
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            if responseError != nil {
                completion(nil, responseError)
            }

            if let data = responseData {
                let decoder = JSONDecoder()
                do {
                    let responce = try decoder.decode(TResponse.self, from: data)
                    completion(responce, nil)
                } catch {
                    completion(nil, error)
                }
            } else {
                completion(nil, nil)
            }
        }
        task.resume()
    }
    
    internal func post<TRequest: Encodable, TResponse: Decodable>(data: TRequest, url: URL, headers: [String: String], completion: @escaping (TResponse?, Error?) -> Void){
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(data)
            request.httpBody = jsonData
        } catch {
            completion(nil, error)
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            if responseError != nil {
                completion(nil, responseError)
            }
            if let data = responseData {
                let decoder = JSONDecoder()
                do {
                    let responce = try decoder.decode(TResponse.self, from: data)
                    completion(responce, nil)
                } catch {
                    completion(nil, error)
                }
            } else {
                completion(nil, nil)
            }
        }
        task.resume()
    }
    
    internal func put<TRequest: Encodable, TResponse: Decodable>(data: TRequest, url: URL, headers: [String: String], completion: @escaping (TResponse?, Error?) -> Void){
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "PUT"
        request.allHTTPHeaderFields = headers
        
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(data)
            request.httpBody = jsonData
        } catch {
            completion(nil, error)
        }

        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            if responseError != nil {
                completion(nil, responseError)
            }
            if let data = responseData {
                let decoder = JSONDecoder()
                do {
                    let responce = try decoder.decode(TResponse.self, from: data)
                    completion(responce, nil)
                } catch {
                    completion(nil, error)
                }
            } else {
                completion(nil, nil)
            }
        }
        task.resume()
    }
    
    internal func delete<TRequest: Encodable, TResponse: Decodable>(data: TRequest, url: URL, headers: [String: String], completion:@escaping (TResponse?, Error?) -> Void){
        var request = URLRequest(url: url)
        
        request.httpMethod = "DELETE"
        request.allHTTPHeaderFields = headers
        
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(data)
            request.httpBody = jsonData
        } catch {
            completion(nil, error)
        }
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            if responseError != nil {
                completion(nil, responseError)
            }
            if let data = responseData {
                let decoder = JSONDecoder()
                do {
                    let responce = try decoder.decode(TResponse.self, from: data)
                    completion(responce, nil)
                } catch {
                    completion(nil, error)
                }
            } else {
                completion(nil, nil)
            }
        }
        task.resume()
    }
}
