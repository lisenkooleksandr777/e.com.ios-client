//
//  AuthenticationController.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import SwiftUI
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class AuthenticationController: UIViewController, LoginButtonDelegate, AuthenticationDelegate, GIDSignInDelegate {

// MARK: - private variables
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    
    private var facebookLoginButton: FBLoginButton?
    private var googleLoginButton: GIDSignInButton?
    
    private let authService = AuthenticationService()
    private let loginManager = LoginManager()
    
    public var rootDelegate : RootViewControllerDelegate!
    //********************************************//
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBackground()
        
        authService.authenticationDelagate = self
        
        // Initialize Google sign-in
        GIDSignIn.sharedInstance().clientID = "81531342117-4o2plepfdo60edq2i0ndp7gjfsp9g5bs.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        
        self.view.backgroundColor = UIColor.white
        self.tryAuth()
    }

// MARK: - Implementation of AuthenticationDelegate
 
    func authenticationDidCompleteSuccess(_ authResult: AuthResult) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)){
            if let t = authResult.token {
                SessionDataHolder.JWTToken = t
                SessionDataHolder.Claims = authResult.userData
                self.stopLoading()
                self.rootDelegate.toContentViewController()
            } else {
                self.authenticationDidCompleteFail("Error during authentication")
            }
        }
    }
    
    func authenticationDidCompleteFail(_ error: String) {
        DispatchQueue.main.async() {
            self.stopLoading()
            let alrt = ErrorAlertFactory.GetErrorAlert(message: error)
            
            let okAction = UIAlertAction(title: LocalizationUtil.GetLocalizationString(id: "OK") , style: UIAlertAction.Style.cancel) { (action) in
                self.addFaceBookLoginButton()
                self.addGoogleLogininButton()
            }
            
            let retryAction = UIAlertAction(title: LocalizationUtil.GetLocalizationString(id: "Retry") , style: UIAlertAction.Style.default) { (action) in
                self.tryAuth()
            }
            
            alrt.addAction(okAction)
            alrt.addAction(retryAction)
    
            self.present(alrt, animated: true, completion: nil)
        }
    }
    
// MARK: - Authentication
    private func showLoading() {
        view.backgroundColor = UIColor.white
        self.activityIndicator.color = UIColor.gray
        view.addSubview(activityIndicator)
        activityIndicator.frame = view.bounds
        activityIndicator.startAnimating()
    }
    
    private func stopLoading() {
        self.activityIndicator.stopAnimating()
    }
    
    private func tryAuth() {
        //TODO: add cacheing of JWT token
        if AccessToken.isCurrentAccessTokenActive {
            self.getFacebooklUserData()
        } else {
            self.addFaceBookLoginButton()
            self.addGoogleLogininButton()
        }
    }
    
    private func authenticateByToken(JWTtoken: String){
        self.authService.authByToken(JWTtoken: JWTtoken)
    }
    
    @objc func customGoogleBookButtonClick(_ sender: UIButton) {
        googleLoginButton?.sendActions(for: .touchUpInside)
    }
    
        
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
          if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
            print("The user has not signed in before or they have since signed out.")
          } else {
            print("\(error.localizedDescription)")
          }
          // [START_EXCLUDE silent]
          NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
          // [END_EXCLUDE]
          return
        }
        self.stopLoading()
        self.authService.authByGoogleUser(googleUserData: user)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      NotificationCenter.default.post(
        name: Notification.Name(rawValue: "ToggleAuthUINotification"),
        object: nil,
        userInfo: ["statusText": "User has disconnected."])
    }
    
    @objc func customFaceBookButtonClick(_ sender: UIButton) {
         facebookLoginButton?.sendActions(for: .touchUpInside)
    }
    
    private func getFacebooklUserData(){
        self.showLoading()
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(normal), email, birthday, gender"])
            .start(completionHandler: { (connection, result, error) -> Void in
                if let err = error {
                    self.authenticationDidCompleteFail(err.localizedDescription)
                } else {
                    guard let userFaceBookData = (result as? [String : AnyObject]) else {
                        self.addFaceBookLoginButton()
                        return
                    }
                    self.authService.authByFaceBookUser(faceBookUserData: userFaceBookData, idToken: AccessToken.current?.tokenString)
                }
            }
        )
    }
    
//MARK: - view settuping
    
    private func addBackground() {
        let view = self.view!
        let backGroundImage = UIImage(named: "login-background")
        let backGroundView = UIImageView(image: backGroundImage)
        
        let maxX =  Int(backGroundView.frame.width)
        let maxY = Int(backGroundView.frame.height)
        for row in 1...5 {
            for column in 1...4 {
                let pointView = UILabel()
                let x = maxX - (24 * column)
                let y = maxY - (24 * row)
                pointView.frame = CGRect(x: x, y: y, width: 6, height: 6)
                pointView.backgroundColor = .white
                pointView.textAlignment = .center
                pointView.textColor = Colors.loginButtonsBackground
                pointView.layer.cornerRadius = 3
                pointView.clipsToBounds = true
                backGroundView.addSubview(pointView)
            }
        }

        view.addSubview(backGroundView)
        
        let maxyBottom = maxY - 18
        for row in 1...3 {
            for column in 1...4 {
                let pointView = UILabel()
                let x = maxX - (24 * column)
                let y = maxyBottom + (24 * row)
                pointView.frame = CGRect(x: x, y: y, width: 6, height: 6)
                pointView.backgroundColor = .gray
                pointView.textAlignment = .center
                pointView.textColor = Colors.loginButtonsBackground
                pointView.layer.cornerRadius = 3
                pointView.clipsToBounds = true
                view.addSubview(pointView)
            }
        }
        
        
        
        let splashImage = UIImage(named: "login-splash")
        let splashView = UIImageView(image: splashImage)
        
        let blockWidth = view.frame.width * 0.5
        let labelView = UILabel()
        labelView.frame = CGRect(x: blockWidth * 0.1, y: splashView.frame.height / 2.1, width: blockWidth, height: blockWidth)
        labelView.backgroundColor = .clear
        labelView.textAlignment = .center
        labelView.textColor = .white
        labelView.lineBreakMode = .byWordWrapping
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.05
        
        let customIconText = NSMutableAttributedString(string: "Welcome", attributes: [
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 40),
            NSAttributedString.Key.backgroundColor: UIColor.clear,
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.kern: 2.02,
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ])
        labelView.attributedText = customIconText
        splashView.addSubview(labelView)
        
        let minX = 12
        let minY = 36
        for row in 0...6 {
            for column in 0...2 {
                let pointView = UILabel()
                let x = minX + (24 * column)
                let y = minY + (24 * row)
                pointView.frame = CGRect(x: x, y: y, width: 6, height: 6)
                pointView.backgroundColor = .white
                pointView.textAlignment = .center
                pointView.textColor = Colors.loginButtonsBackground
                pointView.layer.cornerRadius = 4
                pointView.clipsToBounds = true
                splashView.addSubview(pointView)
            }
        }
        
        view.addSubview(splashView)
    }
    
    private func addGoogleLogininButton() {
        GIDSignIn.sharedInstance()?.delegate = self
        googleLoginButton = GIDSignInButton()
        googleLoginButton?.isHidden = true
        
            
        let buttonHeight = view.frame.height * 0.08;
        let loginButton = createLoginButton(text: "Continue with Google" , icon: "G+", rectangle: CGRect(origin: CGPoint(x: view.frame.width * 0.07, y: view.frame.height * 0.75 + buttonHeight * 0.2), size: CGSize(width: view.frame.width * 0.86, height: buttonHeight)), buttonHeight: buttonHeight)
        
        //adding
        loginButton.addTarget(self, action: #selector(customGoogleBookButtonClick), for: .touchUpInside)
        
        self.view.addSubview(loginButton)
        
    }
    
    private func addFaceBookLoginButton(){
        facebookLoginButton = FBLoginButton()
        facebookLoginButton?.delegate = self
        facebookLoginButton?.isHidden = true
        facebookLoginButton?.delegate = self
        facebookLoginButton?.permissions = ["public_profile", "email", "user_birthday", "user_gender", "user_events"]
          
        let buttonHeight = view.frame.height * 0.08;
        let loginButton = createLoginButton(text: "Continue with Facebook" , icon: "f", rectangle: CGRect(origin: CGPoint(x: view.frame.width * 0.07, y: view.frame.height * 0.75 - buttonHeight), size: CGSize(width: view.frame.width * 0.86, height: buttonHeight)), buttonHeight: buttonHeight)
        
        loginButton.addTarget(self, action: #selector(customFaceBookButtonClick), for: .touchUpInside)
        
        self.view.addSubview(loginButton)
    }
    
    private func createLoginButton(text: String, icon: String, rectangle: CGRect, buttonHeight: CGFloat) -> UIButton {
        let loginButton = UIButton(frame: rectangle)
        loginButton.backgroundColor = Colors.loginButtonsBackground
        loginButton.layer.cornerRadius = loginButton.frame.height * 0.4
        
        let customButtonTitle = NSMutableAttributedString(string: text, attributes: [
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15),
            NSAttributedString.Key.backgroundColor: UIColor.clear,
            NSAttributedString.Key.foregroundColor: UIColor.white
        ])
        loginButton.setAttributedTitle(customButtonTitle, for: .normal)
        loginButton.titleLabel?.textAlignment = .center
        loginButton.setBackgroundColor(color: Colors.loginButtonsBackgroundHover, forState: .highlighted)
        
        let view = UILabel()
        view.frame = CGRect(x: buttonHeight * 0.35, y: buttonHeight * 0.2, width: buttonHeight * 0.6, height: buttonHeight * 0.6)
        view.backgroundColor = .white
        view.text = icon
        view.textAlignment = .center
        view.textColor = Colors.loginButtonsBackground
        view.layer.cornerRadius = buttonHeight * 0.3
        view.clipsToBounds = true
        
        let customIconText = NSMutableAttributedString(string: icon, attributes: [
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16),
            NSAttributedString.Key.backgroundColor: UIColor.clear,
            NSAttributedString.Key.foregroundColor: Colors.loginButtonsBackground,
        ])
        view.attributedText = customIconText
        loginButton.addSubview(view)
        
        return loginButton
    }
    

    
// MARK: - Implementation of LoginButtonDelegate
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if let err = error {
            self.authenticationDidCompleteFail(err.localizedDescription)
        } else if let _ = AccessToken.current {
            self.getFacebooklUserData()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
}
