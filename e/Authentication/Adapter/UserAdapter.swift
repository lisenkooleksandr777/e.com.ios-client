//
//  UserAdapter.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import GoogleSignIn

public class UserAdapter {
    public static func FacebookUserToAuthUser(faceBookUserData: [String: Any], idToken: String?) -> AuthUser {
        let authUser = AuthUser()
        
        authUser.name = (faceBookUserData["name"] as! String)
        authUser.email = (faceBookUserData["email"] as? String)
        authUser.gender = ((faceBookUserData["gender"] as? String))
        authUser.birthday = ((faceBookUserData["birthday"] as? String))
                        
        if let dp = (faceBookUserData["picture"] as? [String : AnyObject]) {
            if let pciture = dp["data"] {
                authUser.pictureUrl = (pciture["url"] as! String)
            }
        }
        
        authUser.provider = "FACEBOOK"
        
        authUser.faceBookToken = FacebookToken()
        authUser.faceBookToken!.tokenString = idToken
        authUser.faceBookToken!.userID = (faceBookUserData["id"] as! String)

        return authUser;
    }
    
    public static func GoogleUserToAuthUser(googleUser: GIDGoogleUser) -> AuthUser {
        let authUser = AuthUser()
        
        authUser.provider = "GOOGLE"
        
        authUser.name = googleUser.profile.name
        authUser.email = googleUser.profile.email
        
        if googleUser.profile.hasImage {
            authUser.pictureUrl = googleUser.profile.imageURL(withDimension: 0)?.absoluteString
        }
        
        
        authUser.googleToken = GoogleToken()
        authUser.googleToken!.userID = googleUser.userID
        authUser.googleToken!.tokenId = googleUser.authentication.idToken
        authUser.googleToken!.tokenString = googleUser.authentication.accessToken
        
        return authUser;
    }
    
    
}
