//
//  AuthenticationService.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import GoogleSignIn

internal class AuthenticationService {
    
    private let authRepository = AuthRepository()
    
    var authenticationDelagate: AuthenticationDelegate?
    
    public func authByFaceBookUser(faceBookUserData: [String: Any], idToken: String?) {
        let authUser = UserAdapter.FacebookUserToAuthUser(faceBookUserData: faceBookUserData, idToken: idToken)
        authRepository.auth(authUser: authUser, callBack: self.authenticationDidComplete)
    }
    
    public func authByGoogleUser(googleUserData: GIDGoogleUser) {
        let authUser = UserAdapter.GoogleUserToAuthUser(googleUser: googleUserData)
        authRepository.auth(authUser: authUser, callBack: self.authenticationDidComplete)
    }
    
    public func authByToken(JWTtoken: String) {
        authRepository.authByToken(JWTtoken: JWTtoken, callBack: self.authenticationDidComplete)
    }
    
    private func authenticationDidComplete(authResponce: AuthResult?, error: Error?){
        if let er = error {
            self.authenticationDelagate?.authenticationDidCompleteFail(er.localizedDescription)
        } else if let response = authResponce {
            self.authenticationDelagate?.authenticationDidCompleteSuccess(response)
        }
    }
}
