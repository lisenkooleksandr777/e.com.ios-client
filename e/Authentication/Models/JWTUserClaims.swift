//
//  JWTUserClaims.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class JWTUserClaims: Decodable  {
    public var id: String?
    public var name: String?
    public var pictureUrl: String?
    public var permissions: String?
}
