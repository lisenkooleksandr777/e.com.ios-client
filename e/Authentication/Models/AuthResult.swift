//
//  AuthResult.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class AuthResult: Decodable {
    public var token: String?
    public var userData: JWTUserClaims?
}
