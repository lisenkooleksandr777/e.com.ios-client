//
//  FacebookToken.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

public class FacebookToken: Decodable, Encodable {
    public var userID: String?
    public var tokenString: String?
}
