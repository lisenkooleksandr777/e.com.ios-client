//
//  AuthCompleteDelegate.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation


protocol AuthenticationDelegate {
    func  authenticationDidCompleteSuccess (_ userData: AuthResult)
    func  authenticationDidCompleteFail (_ error: String)
}
