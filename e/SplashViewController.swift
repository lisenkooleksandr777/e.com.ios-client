//
//  SplashViewController.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/29/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import UIKit

public class SplashViewController: UIViewController {
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.red
        
    }
}
