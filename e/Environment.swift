//
//  Environment.swift
//  e
//
//  Created by Oleksandr Lisenko on 29.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct ImageCacheKey: EnvironmentKey {
    static let defaultValue: ImageCache = TemporaryImageCache()
}

struct EventAnalyticCacheKey: EnvironmentKey {
    static let defaultValue: EventAnalyticCache = TemporaryEventAnalyticCache()
}

extension EnvironmentValues {
    var imageCache: ImageCache {
        get { self[ImageCacheKey.self] }
        set { self[ImageCacheKey.self] = newValue }
    }
    
    var eventAnalyticCache: EventAnalyticCache {
        get { self[EventAnalyticCacheKey.self] }
        set { self[EventAnalyticCacheKey.self] = newValue }
    }
}
