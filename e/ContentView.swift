//
//  ContentView.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/22/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = 0
 
    var body: some View {
        TabView(selection: $selection) {
               
            HomeView()
                .tabItem {
                    VStack {
                        Image("home_icon")
                            .imageScale(.large)
                        
                        Text("Home")
                    }
                }
                .tag(0)
                    
            
            GlobalSearchView(searchServiceDelegate: GlobalSearchService())
                .tabItem {
                    VStack {
                        Image("search_icon")
                        .imageScale(.large)
                        
                        Text("Search")
                        
                    }
                }
                .tag(1)
            
            LocalSearchView()
                .tabItem {
                    VStack {
                        Image("events_icon")
                        .imageScale(.large)
                        
                        Text("Events")
                    }
                }
                .tag(2)
            
            CalendarView()
                .tabItem {
                    VStack {
                        Image("calendar_icon")
                        .imageScale(.large)
                        
                        Text("Calendar")
                    }
                }
                .tag(3)
            
            ProfileView()
                .tabItem {
                    VStack {
                        Image("profile_icon")
                        .imageScale(.large)
                        
                        Text("Profile")
                    }
                }
                .tag(4)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
