//
//  HomeView.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.08.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        Text("Home View!")
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
