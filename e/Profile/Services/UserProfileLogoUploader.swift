//
//  UserProfileLogoUploader.swift
//  e
//
//  Created by Oleksandr Lisenko on 09.11.2019.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation
import Alamofire

class UserProfileLogoUploader {

    /*
    public func uploadFile(event: NewEventViewModel, fileName: String, mimeType: String, callBack:  @escaping (NewEvent) -> (), errorHandler: @escaping (String)->() ) {
        
        let url = URL(string: "\(Configuration.FileSerivceEndPoint!)/upload")!
        
        self.requestWith(endUrl: url.absoluteString, imageData: event.LogoData, fileName: fileName, mimeType: mimeType, event: event.toModel(), callBack: callBack, errorHandler: errorHandler)
    }
    
    private func requestWith(endUrl: String, imageData: Data?, fileName: String, mimeType:String, event: NewEvent, callBack: @escaping (NewEvent) -> (), errorHandler: @escaping (String)->()){
        
        let headers = HttpHeaderBuilder().addMultipartFormDataHeader().addAuthenticationToken().Result()
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let data = imageData{
                multipartFormData.append(data, withName: "file", fileName: fileName, mimeType: mimeType)
            }
        }, usingThreshold: UInt64.init(), to: endUrl, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let err = response.error {
                        errorHandler(err.localizedDescription)
                    }
                    let decoder = JSONDecoder()
                    do {
                        /*
                        let result = try decoder.decode(Response<String>.self, from: response.data!)
                        
                        if(result.IsSuccess){
                            event.addLogo(logurl: result.Result!)
                            callBack(event)
                        }
                        else {
                            if let msg = result.Error?.Message {
                                errorHandler(msg)
                            }
                        }
                        */
                    } catch {
                        errorHandler(error.localizedDescription)
                    }
                }
            case .failure(let error):
                errorHandler(error.localizedDescription)
            }
        }
    }
 */
}
