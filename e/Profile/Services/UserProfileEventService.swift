//
//  UserProfileService.swift
//  e
//
//  Created by Oleksandr Lisenko on 09.11.2019.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation


public class UserProfileEventService {

    private let eventRepository = UserProfileEventRepository()
    private let logoUploader = UserProfileLogoUploader()

/*
    func createEvent(newEventViewModel: NewEventViewModel, callBack: @escaping (NewEvent) -> (), errorHandler: @escaping (String) -> ()) {
        self.logoUploader.uploadFile(event: newEventViewModel , fileName: ".png" , mimeType: "image/png",
            callBack:  { eventToSave in
                self.eventRepository.createEvent(newEvent: eventToSave) { ( creationResult, error) in
                    if let er = error {
                        errorHandler(er.localizedDescription)
                    } else if let response = creationResult {
                        if response.IsSuccess {
                            callBack(response.Result!)
                        } else {
                            errorHandler(response.Error!.Message!)
                        }
                    }
                }
        }, errorHandler: errorHandler
        )
    }
        
    func ownEventPage(page: Int, callBack: @escaping ([EventPreview]?) -> (), errorHandler: @escaping (String) -> ()) {
        self.eventRepository.ownEventPage(page: page, callBack: { serverResponce, error in
            if let er = error {
                errorHandler(er.localizedDescription)
            } else if let response = serverResponce {
                if response.IsSuccess {
                    callBack(response.Result)
                } else {
                    errorHandler(response.Error!.Message!)
                }
            }
        })
    }
    */
}
