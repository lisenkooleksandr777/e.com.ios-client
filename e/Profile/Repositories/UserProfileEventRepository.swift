//
//  UserProfileRepository.swift
//  e
//
//  Created by Oleksandr Lisenko on 09.11.2019.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class UserProfileEventRepository: BaseRepository {
    
    private let createEventEndPoint = "/events/create"
    private let getOwnEventPageEndPoint = "/events/own/page/"
    /*
    //function will create new user in case user does not exist in the system or will authenticate existing one
    func createEvent(newEvent: NewEvent, callBack: @escaping (Response<NewEvent>?, Error?) -> Void) {
        
        let url = URL(string: "\(Configuration.AccountSerivceEndPoint!)\(self.createEventEndPoint)")!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        super.post(data: newEvent, url: url, headers: headers, completion: callBack)
    }
    
    //function will create new user in case user does not exist in the system or will authenticate existing one
    func ownEventPage(page: Int, callBack: @escaping (Response<[EventPreview]>?, Error?) -> Void) {
        
        let url = URL(string: "\(Configuration.AccountSerivceEndPoint!)\(self.getOwnEventPageEndPoint)\(page)")!
        
        let headers = HttpHeaderBuilder().addJsonContentType().addAuthenticationToken().Result()
        super.get(url: url, headers: headers, completion: callBack)
    }
 */
}
