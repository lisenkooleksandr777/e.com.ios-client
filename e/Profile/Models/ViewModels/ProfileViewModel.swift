//
//  ProfileViewModel.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.08.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import Foundation

class ProfileViewModel: ObservableObject {
    @Published var userName: String =  SessionDataHolder.Claims.name ?? "Loading"
    @Published var userAvatarURL: String = SessionDataHolder.Claims.pictureUrl ?? "https://www.pngitem.com/pimgs/m/294-2947257_interface-icons-user-avatar-profile-user-avatar-png.png"
    
//MARK: - Localization strings
    @Published var iCreatedLabel = "My events"
    @Published var newEventLabel = "New event"
    @Published var gonnaVisitLabel = "I am gonna visit"
    @Published var invintationsLabel = "My invintation"
    @Published var likedEventsLabel = "Favourite events"
    @Published var myTicketsLabel = "MY TICKETS"
    
    @Published var logOutLabel = "Log Out"
    @Published var followersLabel = "Followers"
    @Published var followingsLabel = "Followings"
    
//MARK: - Events
    func addEvent() {
        
    }
}
