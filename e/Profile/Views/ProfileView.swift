//
//  ProfileView.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/29/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    
    @Environment(\.imageCache) var cache: ImageCache
    @ObservedObject var viewModel: ProfileViewModel = ProfileViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    AsyncImage(
                        url:  URL(string: viewModel.userAvatarURL)!,
                        cache: self.cache,
                        placeholder: Text("Loading ..."),
                        configuration: { $0.resizable() }
                    )
                    .frame(width: 150.0, height: 150.0)
                    .cornerRadius(20)
                    .overlay(RoundedRectangle(cornerRadius: 20)
                    .stroke(Color.init(red: 0.898, green: 0.91, blue: 0.937), lineWidth: 2))
                    
                    Spacer()
                    
                    VStack {
                        Text(viewModel.userName)
                        .fontWeight(.semibold)
                        .font(.callout)
                        .padding()
                        
                        Button(action: self.viewModel.addEvent) {
                            HStack {
                                Image("plus_icon")
                                .padding(.vertical, 15)
                                .padding(.leading, 20)
                                
                                Text(viewModel.newEventLabel)
                                .foregroundColor(Color.init(red: 0.02, green: 0.412, blue: 0.957))
                                .fontWeight(.semibold)
                                .font(.caption)
                                .padding(.trailing, 20)
                            }
                            .cornerRadius(20)
                            .overlay(RoundedRectangle(cornerRadius: 20)
                            .stroke(Color.init(red: 0.02, green: 0.412, blue: 0.957), lineWidth: 1))
                        }
                    }
                    
                    Spacer(minLength: 10)
                }
                .padding(.leading, 15)
                
                
                Spacer(minLength: 25)
                
                ScrollView {
                    VStack {
                        EventListLinkView(icon: "star_icon", label: viewModel.iCreatedLabel,paddinLeft: 15, paddingRight: 10)
                                
                        EventListLinkView(icon: "gonnavisit_icon", label: viewModel.gonnaVisitLabel,paddinLeft: 17, paddingRight: 10)
          
                        EventListLinkView(icon: "people_icon", label: viewModel.invintationsLabel, paddinLeft: 17, paddingRight: 10)
                          
                        EventListLinkView(icon: "heartfilled_icon", label: viewModel.likedEventsLabel, paddinLeft: 14, paddingRight: 10)
                    }
      
                    Spacer(minLength: 20)
                    
                    Button(action: self.viewModel.addEvent) {
                          HStack {
                              Image("ticket_icon")
                                .padding(.vertical, 15)
                                .padding(.leading, 15)
                                .foregroundColor(Color.white)
                            
                            Text(viewModel.myTicketsLabel)
                                .fontWeight(.semibold)
                                .font(.caption)
                                .padding(.trailing, 15)
                                .foregroundColor(Color.white)
                          }
                          .background(Color.init(red: 0.02, green: 0.412, blue: 0.957))
                          .cornerRadius(20)
                          .overlay(RoundedRectangle(cornerRadius: 20)
                          .stroke(Color.init(red: 0.02, green: 0.412, blue: 0.957), lineWidth: 1))
                      }
                    
                    Spacer(minLength: 30)
                   
                    VStack {
                    
                        UserListLinkView(icon: "followers_icon", label: viewModel.followingsLabel, paddinLeft: 15, paddingRight: 10)
                        
                        UserListLinkView(icon: "followings_icon", label: viewModel.followersLabel, paddinLeft: 15, paddingRight: 10)
                        
                        UserListLinkView(icon: "logout_icon", label: viewModel.logOutLabel, paddinLeft: 15, paddingRight: 10)
                    }
                    
                    Spacer()
                }
            }
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
