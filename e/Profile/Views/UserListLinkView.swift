//
//  UserListLink.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.08.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct UserListLinkView: View {
    @State var icon: String
    @State var label: String
    @State var paddinLeft: CGFloat
    @State var paddingRight: CGFloat
    
    var body: some View {
        VStack {
        HStack {
            NavigationLink(destination: UserListView()) {
                HStack {
                    Image(icon)
                        .padding(.trailing, 5)
                
                    Text(label)
                        .foregroundColor(Color.init(red: 0.169, green: 0.204, blue: 0.271))
                        .fontWeight(.medium)
                        .font(.caption)
                                        
                    Spacer()
                    Image("rightnavigationarrow_icon")
                }
            }
            .padding(.leading, paddinLeft)
            .padding(.trailing, paddingRight)
            
            Spacer()
        }
        Rectangle()
            .frame(height: 1.0, alignment: .bottom)
            .foregroundColor(Color.init(red: 0.898, green: 0.91, blue: 0.937))
            .padding(.vertical, 5)
        }.padding(.trailing, paddingRight)
    }
}
