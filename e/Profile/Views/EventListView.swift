//
//  EventListView.swift
//  e
//
//  Created by Oleksandr Lisenko on 24.08.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct EventListView: View {
    var body: some View {
        Text("Event list view")
    }
}

struct EventListView_Previews: PreviewProvider {
    static var previews: some View {
        EventListView()
    }
}
