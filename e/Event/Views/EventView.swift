//
//  EventView.swift
//  e
//
//  Created by Oleksandr Lisenko on 28.05.2020.
//  Copyright © 2020 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct EventView: View {
    var previewModel: EventPreviewViewModel
    
    var body: some View {
        Text(previewModel.name)
    }
}
