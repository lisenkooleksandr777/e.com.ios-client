//
//  LocalSearchView.swift
//  e
//
//  Created by Oleksandr Lisenko on 9/30/19.
//  Copyright © 2019 Oleksandr Lisenko. All rights reserved.
//

import SwiftUI

struct LocalSearchView: View {
    var body: some View {
        Text("Local search")
    }
}

struct LocalSearchView_Previews: PreviewProvider {
    static var previews: some View {
        LocalSearchView()
    }
}
